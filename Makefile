all:

serve:
	jekyll serve

ci:
	git ci . -m "Updated website."
	git push

ssh:
	ssh ci@algoplateausaclay-dowork.ci

ics: seminar/calendar.ics

seminar/calendar.ics: _data/seminar.yml _scripts/sem2ical.py
	cd _scripts && ./sem2ical.py
