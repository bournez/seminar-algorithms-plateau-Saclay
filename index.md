---
layout: page
title: Le séminaire "Algorithmique du Plateau de Saclay"
subtitle: Algorithms of Saclay Plateau Seminar
---

Le séminaire "Algorithmique du Plateau de Saclay" a été  financé pour
partie avec l’aide du labex Digicosme, commun à plusieurs laboratoires
dont le LIX, le LRI, le laboratoire IBISC, et les laboratoires
Li-PARAD et ALMOST.

Il a continué à exister même après ce soutien financier.

Actuellement, il implique le LMF (principalment le pôle Modèles), le LISN (principalement équipe GALaC), le
LIX (principalement l'équipe AlCo).

Le séminaire porte sur l’ensemble des thématiques du domaine de l’algorithmique, et notamment (liste non exhaustive):

- l’algorithmique de la théorie de calculabilité ;
- la théorie de la complexité ;
- la théorie algorithmique des jeux ;
- l’optimisation discrète ;
- l’algorithmique distribuée.



Aller ici pour les annonces du séminaire  [séminaire](seminar).
