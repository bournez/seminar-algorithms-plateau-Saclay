#!/usr/bin/python
# -*- coding: utf-8 -*-
import yaml
import locale
from datetime import datetime, timedelta
import dateutil.parser
from icalendar import Calendar, Event
import random
from pytz import timezone

#locale.setlocale(locale.LC_TIME, "en_US.UTF-8")
tz = timezone('Europe/Paris')
  
def capitalize_first(s):
    return "" if s == "" else (s[0].capitalize() + s[1:])
 
next_sem = "TBA"
datestring = ""
room = ""
speaker = ""
abstract= ""
title = ""
prev_sem = "Following seminars: TBA"
prevsem_next = "Following seminars: TBA"

calendar = Calendar()
calendar.add('version', '2.0')
calendar.add('prodid', '-//Olivier Bournez//Algorithms of Saclay Plateau seminar//EN')
calendar.add('x-wr-calname', 'Algorithms of Saclay Plateau seminar')

f = open('../_data/seminar.yml')
events = yaml.load_all(f, Loader=yaml.FullLoader)

events = events.next()

for data in events:

    #save data

    datestringnext = datestring
    roomnext = room
    speakernext = speaker
    titlenext = title
    abstractnext = abstract
    next_semnext = next_sem
 
    
    # Compute fields
   # team = data['team']
    try: 
        room = data['room']
        if not room.startswith("http"):
            room = "room " + ("TBA" if room == None else room.encode("utf-8"))
    except:
        room = "room (TBA) "
    website = data['website']
    website = "" if website == None else website.encode("utf-8")
    speaker = data['speaker'].encode("utf-8")
    lab = data['lab']
    lab = "" if lab == None else lab.encode("utf-8")
    picture = data['picture']
    picture = "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Anonymous.svg/433px-Anonymous.svg.png" if picture == None else picture
    picture = "<a href=\"{website}\"><img src=\"{picture}\" class=\"picture\" alt=\"{speaker}\"/></a>".format(speaker=speaker,website=website,picture=picture.encode("utf-8"))
    picturetweet = data['picture']
    picturetweet = "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Anonymous.svg/433px-Anonymous.svg.png" if picturetweet == None else picturetweet
    #picturetweet = "<a href=\"{website}\"><img src=\"{picture}\" class=\"picture\" alt=\"{speaker}\"/></a>".format(speaker=speaker,website=website,picture=picture.encode("utf-8"))
    date = dateutil.parser.parse(data['date'],dayfirst=False)
    date = tz.localize(date)
    datestring = date.strftime('%A %d %B %Y at %Hh%M')
    title = data['title']
    title = "TBA" if title == None else title.encode("utf-8")
    abstract = data['abstract']
    abstract = "TBA" if abstract == None else abstract.encode("utf-8")


    # Generate mail
    if date - datetime.now(tz) > timedelta():
        f = open("template.mail")
        mail = f.read()
        f.close()
        mail = mail.replace("DATE", capitalize_first(datestring))
        mail = mail.replace("ROOM", room)
        mail = mail.replace("SPEAKER", speaker)
        mail = mail.replace("TITLE", title)
        mail = mail.replace("ABSTRACT", abstract)
        mail = mail.replace("NEXTSEM", next_sem)
        f = open("../seminar/mail.txt", "w")
        f.write(mail)
        f.close()
        #

    prev_semnextnext = prevsem_next
    prev_semnext = prev_sem
    prev_sem = "Recall that next seminar will be on " + datestring + " by " + speaker
    prev_sem += (": " + title) if title != "TBA" else ""
    prev_sem += "."

    # Generate possibly next talk mail
    if date - datetime.now(tz) > timedelta():
        f = open("template-next.mail")
        mail = f.read()
        f.close()
        mail = mail.replace("PREVSEM", prev_sem)
        mail = mail.replace("DATENEXT", capitalize_first(datestringnext))
        mail = mail.replace("ROOMNEXT", roomnext)
        mail = mail.replace("SPEAKERNEXT", speakernext)
        mail = mail.replace("TITLENEXT", titlenext)
        mail = mail.replace("ABSTRACTNEXT", abstractnext)
        mail = mail.replace("NEXTSEMNEXT",prev_semnextnext)
        f = open("../seminar/mailnext.txt", "w")
        f.write(mail)
        f.close()



    # Generate tweet
    if date - datetime.now(tz) > timedelta():
        f = open("template.tweet")
        tweet = f.read()
        f.close()
        tweet = tweet.replace("DATE", capitalize_first(datestring))
        tweet = tweet.replace("ROOM", room)
        tweet = tweet.replace("SPEAKER", speaker)
        tweet = tweet.replace("TITLE", title)
        tweet = tweet.replace("ABSTRACT", abstract)
        tweet = tweet.replace("NEXTSEM", next_sem)
        tweet = tweet.replace("PICTURETWEET", picturetweet)
        f = open("../seminar/tweet.txt", "w")
        tweet = tweet.replace("\\'e","é")
        f.write(tweet)
        f.close()
   

    next_sem = "Next seminar will be on " + datestring + " by " + speaker
    next_sem += (": " + title) if title != "TBA" else ""
    next_sem += "."

    # Add ical event
    event = Event()
    event.add('organizer', 'MAILTO:olivier.bournez@lix.polytechnique.fr')
    event.add('uid', random.random())
    event.add('summary', speaker + ' / Algorithms of Saclay Plateau seminar')
    event.add('dtstart', date)
    event.add('dtend', date + timedelta(hours=1))
    event.add('dtstamp', datetime.now(tz))
    event.add('location', room)
    event.add('description', speaker + ": " + title + "\n\n" + abstract)
    calendar.add_component(event)

f.close()

f = open("../seminar/calendar.ics", "w")
f.write(calendar.to_ical())
f.close()
